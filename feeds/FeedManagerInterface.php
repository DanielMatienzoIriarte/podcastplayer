<?php

namespace feeds;

interface FeedManagerInterface
{
    public function readFeed(string $feed): array;
}
