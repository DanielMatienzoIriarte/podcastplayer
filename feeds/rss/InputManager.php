<?php

namespace feeds\rss;

use feeds\FeedManagerInterface;
use feeds\FeedReaderInterface;

class InputManager implements FeedManagerInterface
{
    private FeedReaderInterface $inputReader;

    public function __construct(FeedReaderInterface $inputReader) {
        $this->inputReader = $inputReader;
    }

    public function readFeed(string $feed): array
    {
        return $this->inputReader->readFeed($feed);
    }
}
