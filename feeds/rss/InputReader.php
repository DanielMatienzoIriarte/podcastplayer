<?php

namespace feeds\rss;

use feeds\FeedReaderInterface;

class InputReader implements FeedReaderInterface
{
    private RssReader $rssReader;

    public function __construct(RssReader $rssReader) {
        $this->rssReader = $rssReader;
    }

    public function readFeed(string $feed): array
    {
        $feedDataRaw = $this->rssReader::readFeed($feed);

        return $this->formatFeedData($feedDataRaw);
    }

    private function formatFeedData(string $feedDataRaw): array
    {
        $feedData = array();

        $splittedFeedData = explode("<br>", $feedDataRaw);
        foreach ($splittedFeedData as $splittedFeedDatum) {
            $feedDatum = explode(" :: ", $splittedFeedDatum);
            $feedData[] = array('title' => $feedDatum[0], 'link' => $feedDatum[1], 'description' => $feedDatum[2], 'pubDate' => $feedDatum[3]);
        }

        return $feedData;
    }
}
