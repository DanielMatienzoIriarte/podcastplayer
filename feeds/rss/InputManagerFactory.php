<?php

namespace feeds\rss;

use core\FeedFactory;
use feeds\FeedManagerInterface;
use feeds\FeedReaderInterface;

class InputManagerFactory extends FeedFactory
{
    public function __construct() {
    }

    public function create(): FeedManagerInterface {
        $inputReader = $this->getRssReader();

        return new InputManager($inputReader);
    }

    private function getRssReader(): FeedReaderInterface {
        $inputReaderFactory = new InputReaderFactory();

        return $inputReaderFactory->create();
    }
}
