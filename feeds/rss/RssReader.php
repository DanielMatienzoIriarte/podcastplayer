<?php

namespace feeds\rss;

use feeds\FeedReaderInterface;
use DOMDocument;

class RssReader
{
    public function rssReader() {
    }

    public static function readFeed(string $feed): string {
        $result = '';

        $domOBJ = new DOMDocument();
        $domOBJ->load($feed);

        $elements = $domOBJ ->getElementsByTagName("item");

        foreach($elements as $data)
        {
            $title = $data->getElementsByTagName("title")->item(0)->nodeValue;
            $description = $data->getElementsByTagName("description")->item(0)->nodeValue;
            $link = $data->getElementsByTagName("link")->item(0)->nodeValue;
            $pubDate = $data->getElementsByTagName("pubDate")->item(0)->nodeValue;
            $result .= "$title :: $link :: $description :: $pubDate" . "<br>";
        }

        return $result;
    }
}
