<?php

namespace feeds\rss;

use core\FeedReaderFactory;
use feeds\FeedReaderInterface;
use feeds\file\FileReader;

class InputReaderFactory extends FeedReaderFactory
{
    public function create(): FeedReaderInterface
    {
        $rssReader = new RssReader();

        return new InputReader($rssReader);
    }
}
