<?php

namespace feeds\file;

use core\FeedFactory;
use feeds\FeedManagerInterface;
use feeds\FeedReaderInterface;

class FileManagerFactory extends FeedFactory
{
    public function __construct() {
    }

    public function create(): FeedManagerInterface {
        $fileReader = $this->getFileReader();

        return new fileManager($fileReader);
    }

    private function getFileReader(): FeedReaderInterface {
        $fileReaderFactory = new FileReaderFactory();

        return $fileReaderFactory->create();
    }
}
