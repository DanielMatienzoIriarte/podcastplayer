<?php

namespace feeds\file;

use feeds\FeedReaderInterface;
use feeds\rss\RssReader;

class FileReader implements FeedReaderInterface
{
    private RssReader $rssReader;
    private array $fileFeeds = array();

    public function __construct(RssReader $rssReader) {
        $this->rssReader = $rssReader;
    }

    public function readFeed(string $feed): array
    {
        $feedDataRaw = '';
        $rssList = $this->readFile($feed);

        foreach ($rssList as $rssElement) {
            $rssElement = $this->stripNewLine($rssElement);
            $feedDataRaw .= $this->rssReader::readFeed($rssElement);
        }

        return $this->formatFeedData($feedDataRaw);
    }

    private function readFile(string $fileName): array {
        $feedsFile = fopen($fileName, "r") or die("Unable to open " . $fileName);

        while(!feof($feedsFile)) {
            $this->fileFeeds[] = fgets($feedsFile);
        }

        fclose($feedsFile);

        return $this->fileFeeds;
    }

    private function stripNewLine(string $rssElement): string {
        return str_replace("\n", "", $rssElement);
    }

    private function formatFeedData(string $feedDataRaw): array
    {
        $feedData = array();

        $splittedFeedData = explode("<br>", $feedDataRaw);
        foreach ($splittedFeedData as $splittedFeedDatum) {
            $feedDatum = explode(" :: ", $splittedFeedDatum);
            $feedData[] = array('title' => $feedDatum[0], 'link' => $feedDatum[1], 'description' => $feedDatum[2], 'pubDate' => $feedDatum[3]);
        }

        return $feedData;
    }
}
