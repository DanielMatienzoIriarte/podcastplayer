<?php

namespace feeds\file;

use core\FeedReaderFactory;
use feeds\FeedReaderInterface;
use feeds\rss\RssReader;

class FileReaderFactory extends FeedReaderFactory
{
    public function create(): FeedReaderInterface
    {
        $rssReader = new RssReader();

        return new FileReader($rssReader);
    }
}
