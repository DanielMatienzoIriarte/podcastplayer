<?php

namespace feeds\file;

use feeds\FeedManagerInterface;
use feeds\FeedReaderInterface;

class FileManager implements FeedManagerInterface
{
    private FeedReaderInterface $fileReader;

    public function __construct(FeedReaderInterface $fileReader) {
        $this->fileReader = $fileReader;
    }

    public function readFeed(string $feed): array
    {
        return $this->fileReader->readFeed($feed);
    }
}
