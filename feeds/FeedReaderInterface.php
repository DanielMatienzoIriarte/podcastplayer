<?php

namespace feeds;

interface FeedReaderInterface
{
    public function readFeed(string $feed): array;
}
