<?php

    include_once 'PodCastManager.php';
    include_once __DIR__ . '/core/FeedFactory.php';
    include_once __DIR__ . '/feeds/FeedManagerInterface.php';
    include_once __DIR__ . '/feeds/FeedReaderInterface.php';
    include_once __DIR__ . '/feeds/file/FileManagerFactory.php';
    include_once __DIR__ . '/feeds/rss/InputManagerFactory.php';
    include_once __DIR__ . '/feeds/FeedManagerInterface.php';
    include_once __DIR__ . '/feeds/rss/RssReader.php';
    include_once __DIR__ . '/feeds/file/FileReader.php';
    include_once __DIR__ . '/feeds/rss/InputReader.php';
    include_once __DIR__ . '/core/FeedReaderFactory.php';
    include_once __DIR__ . '/feeds/file/FileReaderFactory.php';
    include_once __DIR__ . '/feeds/rss/InputReaderFactory.php';
    include_once __DIR__ . '/feeds/rss/InputManager.php';
    include_once __DIR__ . '/feeds/file/FileManager.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
    	<link rel="stylesheet" href="static/style.css" type="text/css" media="screen" charset="utf-8">
        <script type="application/javascript" src="static/scripts.js"></script>
        <title>Podcast Player</title>
    </head>
	<?php
        $inputManager = PodCastManager::getInputManager();

        if (!empty($_POST['urlFeed'])) {
            $feedsList = $inputManager->readFeed($_POST['urlFeed']);
            $listTitle = "Input's Rss";
        } else {
            $fileManager = PodCastManager::getFileManager();
            $feedsList = $fileManager->readFeed('feeds.txt');
            $listTitle = "File's Rss";
        }

        unset($_POST['urlFeed']);
	?>
    <body>
    	<div id="main">
            <div>
                <h2>Input an rss URL</h2>
                <form action="index.php" method="post">
                    <input id="urlFeed" name="urlFeed" type="text">
                    <input type="submit" value="View!!">
                </form>
            </div>

    		<div id="rssFileList">
                <select id="feedList">
                    <option>Select Feed</option>
                    <?php
                        foreach ($feedsList as $feedItem) {
                            echo '<option value="'. $feedItem['link'] .'">' . $feedItem['title'] . '</option>';
                        }
                    ?>
    			</select>
                <button id="selectFeed" onclick="loadFeedData(document.querySelector('#feedList').value);return false;">Select</button>
    		</div>
            <div id="feedData">
                <div id="feedTitle">
                </div>
                <div id="feedUrl">
                </div>
                <div id="feedDescription">
                </div>
                <div id="publishDate">
                </div>
            </div>
        </div>
    </body>
</html>
        