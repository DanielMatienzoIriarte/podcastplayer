<?php

    namespace modules;

    use DateTime;

    class rssFeed
    {
        private string $title;
        private string $description;
        private string $url;
        private \DateTime $pubDate;

        public function __construct(){
            
        }

        public function getTitle(): string {
            return $this->title;
        }

        public function setTitle(string $title) {
            $this->title = $title;
        }

        public function getDescription(): string {
            return $this->description;
        }

        public function setDescription(string $description) {
            $this->description = $description;
        }

        public function getUrl(): string {
            return $this->url;
        }

        public function setUrl(string $url) {
            $this->url = $url;
        }

        public function function_name(): DateTime {
            return $this->pubDate;
        }

        public function setPubDate(DateTime $pubDate) {
            $this->pubDate = $pubDate;
        }
    }
