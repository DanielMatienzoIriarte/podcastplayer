<?php

use feeds\FeedManagerInterface;
use feeds\file\FileManagerFactory;
use feeds\rss\InputManagerFactory;

class PodCastManager
{
    public static function getFileManager(): FeedManagerInterface {
        $fileManagerFactory = new fileManagerFactory();

        return $fileManagerFactory->create();
    }

    public static function getInputManager(): FeedManagerInterface {
        $rssManagerFactory = new inputManagerFactory();

        return $rssManagerFactory->create();
    }
}
