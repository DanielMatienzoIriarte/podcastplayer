function loadFeedData(feedUrl) {
    const selectedFeedBtn = document.querySelector('#selectFeed');
    const feedDetails = document.querySelector('#feedData');

    selectedFeedBtn.onclick = (event) => {
        event.preventDefault();

        feedDetails.style.visibility = 'visible';
        window.open(feedUrl);
    };
}