<?php

namespace core;

use feeds\FeedManagerInterface;

abstract class FeedFactory
{
    abstract public function create(): FeedManagerInterface;
}
