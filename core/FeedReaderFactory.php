<?php

namespace core;

use feeds\FeedReaderInterface;

abstract class FeedReaderFactory
{
    abstract public function create(): FeedReaderInterface;
}
